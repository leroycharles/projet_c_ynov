//
// Created by leroycharles on 08/03/2020.
//

#include "combat.h"

/**
 * \brief CCharge l'évènement combat suivant la position du joueur sur le borde et le charge dans le buffer
 */
void eventCombat(Board *mPoint, Perso *Player, Buffer *bPoint) {
    Position pos = Player->position;
    int value = read_board(mPoint, pos);
    switch (value) {
        case 2:
            //Add Context of event 2
            readText(1, 2, bPoint);
            readAscii(2);
            break;
        case 3:
            //Add Context of event 3
            readText(1, 3, bPoint);
            readAscii(3);
            break;
        case 4:
            //Add Context of event 4
            readText(1, 4, bPoint);
            readAscii(4);
            break;
        case 5:
            //Add Context of event 5
            readText(1, 5, bPoint);
            readAscii(5);
            break;
        case 6:
            //Add Context of event 6
            readText(1, 6, bPoint);
            readAscii(6);
            break;
        case 7:
            //Add Context of event 7
            readText(1, 7, bPoint);
            readAscii(7);
            break;
        case 8:
            //Add Context of event 8
            readText(1, 8, bPoint);
            readAscii(8);
            break;
        case 9:
            //Add Context of event 9
            readText(1, 9, bPoint);
            readAscii(9);
            break;
        default:
            //Add Context of event BOSS
            readText(1, 10, bPoint);
            readAscii(10)
            break;
    }
    TMP *tPlayer = initTMP(Player, 0);
    TMP *tMonster = initTMP(Player, 1);

    switch (combat(tPlayer, tMonster, bPoint)) {
        case 0:
            addLineBuffer(bPoint, "Vous vous retournez pour fuir et vous faites décapiter.");
        case 1:
            //Call for gameOver
            break;
        default:
            printBuffer(bPoint);
            userInput(128, bPoint); //replace 128 by menu for combat
            update_board(mPoint, pos);
            break;
    }
}

int combat(TMP *Player, TMP *Monster, Buffer *bPoint) {
    int stopReason = 0;
    int stopBool = 1;
    int buffer = 0;
    while (stopBool) {
        buffer = 0;
        buffer = attack(Monster, Player);
        if (buffer == 0) {
            addLineBuffer(bPoint, "Coup Monstre Raté !");
        } else if (buffer == 1) {
            addLineBuffer(bPoint, "Coup Monstre Réussi !");
        } else {
            addLineBuffer(bPoint, "Coup Monstre Critique");
        }
        recapPerso(bPoint, Monster, 0);
        recapPerso(bPoint, Player, 1);
        buffer = userInput(128, bPoint);
        switch (buffer) {
            case 0:
                //Attack
                buffer = attack(Player, Monster);
                if (buffer == 0) {
                    addLineBuffer(bPoint, "Coup Joueur Raté !");
                } else if (buffer == 1) {
                    addLineBuffer(bPoint, "Coup Joueur Réussi !");
                } else {
                    addLineBuffer(bPoint, "Coup Joueur Critique");
                }
                break;
            case 1:
                //Potion Health
                if (Player->potions[0] != 0) {
                    addLineBuffer(bPoint, "Potion de vie utilisée");
                    bhealthTMP(Player);
                }
                break;
            case 2:
                //Potion Attack
                if (Player->potions[1] != 0) {
                    addLineBuffer(bPoint, "Potion d'attaque utilisée");
                    battackTMP(Player);
                }
                break;
            case 3:
                //Potion Defense
                if (Player->potions[2] != 0) {
                    addLineBuffer(bPoint, "Potion de défence utilisée");
                    bdefenseTMP(Player);
                }
                break;
            default:
                stopBool = 0;
                stopReason = 0;
                break;
        }
        if (Monster->health <= 0) {
            stopBool = 0;
            stopReason = 1;
        } else if (Player->health <= 0) {
            stopBool = 0;
            stopReason = 2;
        }
    }

    return stopReason;
}

void recapPerso(Buffer *bPoint, TMP *Perso, int type) {
    char bufferInt[8], bufferText[128];

    addLineBuffer(bPoint, type ? "STATS JOUEUR:" : "STATS MONSTRE:");

    sprintf(bufferInt, "%d", Perso->attack);
    sprintf(bufferText, "%s", "Attaque: ");
    addLineBuffer(bPoint, strcat(bufferText, bufferInt));

    sprintf(bufferInt, "%d", Perso->defense);
    sprintf(bufferText, "%s", "Défence: ");
    addLineBuffer(bPoint, strcat(bufferText, bufferInt));

    sprintf(bufferInt, "%d", Perso->health);
    sprintf(bufferText, "%s", "Point de vie: ");
    addLineBuffer(bPoint, strcat(bufferText, bufferInt));

    sprintf(bufferInt, "%d", Perso->level);
    sprintf(bufferText, "%s", "Niveau: ");
    addLineBuffer(bPoint, strcat(bufferText, bufferInt));

    if (type) {
        for (int i = 0; i < 3; ++i) {
            sprintf(bufferInt, "%d", Perso->potions[i]);
            switch (i) {
                case 1:
                    sprintf(bufferText, "%s", "Potion d'attaque: ");
                    break;
                case 2:
                    sprintf(bufferText, "%s", "Potion de défence: ");
                    break;
                default:
                    sprintf(bufferText, "%s", "Potion de vie: ");
                    break;
            }
            addLineBuffer(bPoint, strcat(bufferText, bufferInt));
        }
    }
}