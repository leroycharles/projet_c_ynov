//
// Created by leroycharles on 08/03/2020.
//

#include "store.h"


int upgradeWeapon(Perso *Player, int cost) {
    if (Player->money - cost > 0) {
        Player->money -= cost;
        Player->sword += 100;
        return 0;
    } else {
        return 1;
    }
}

int upgradeArmor(Perso *Player, int cost) {
    if (Player->money - cost > 0) {
        Player->money -= cost;
        Player->armor += 1000;
        return 0;
    } else {
        return 1;
    }
}

int upgradeShield(Perso *Player, int cost) {
    if (Player->money - cost > 0) {
        Player->money -= cost;
        Player->shield += 100;
        return 0;
    } else {
        return 1;
    }
}

int upgradeXP(Perso *Player, int cost) {
    if (Player->money - cost > 0) {
        Player->money -= cost;
        Player->xp += 1000;
        return 0;
    } else {
        return 1;
    }
}

int upgradePotion(Perso *Player, int cost, int type) {
    if (Player->money - cost > 0) {
        Player->money -= cost;
        Player->potions[type]++;
        return 0;
    } else {
        return 1;
    }
}