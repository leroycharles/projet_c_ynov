//
// Created by leroycharles on 08/03/2020.
//

#ifndef PROJET_C_YNOV_MOVE_H
#define PROJET_C_YNOV_MOVE_H
#include "../misc/board.h"

void eventMove(Board *mPoint, const Position pos, Buffer *bPoint);
#endif //PROJET_C_YNOV_MOVE_H
