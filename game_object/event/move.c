//
// Created by leroycharles on 08/03/2020.
//

#include "move.h"

/**
 * \brief Charge l'évènement texte de lieu suivant la position du joueur sur le borde et le charge dans le buffer
 */
void eventMove(Board *mPoint, const Position pos, Buffer *bPoint) {
    int value = read_board(mPoint, pos);
    if (value == 11) {
        //Load Text for Forest
        readText(1, 11, bPoint);
        readAscii(11);
    } else if (value == 12) {
        //Load Text for Field
        readText(1, 12, bPoint);
        readAscii(12);
    } else {
        //Load Text for Road
        readText(1, 13, bPoint);
        readAscii(13);
    }
    printBuffer(bPoint);
    userIntput("", 0);
    update_board(mPoint, pos);
}