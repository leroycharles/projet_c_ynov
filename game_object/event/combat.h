//
// Created by leroycharles on 08/03/2020.
//

#ifndef PROJET_C_YNOV_COMBAT_H
#define PROJET_C_YNOV_COMBAT_H

#include "../misc/board.h"

void eventCombat(Board *mPoint, Perso *Player, Buffer *bPoint);

int combat(TMP *Player, TMP *Monster, Buffer *bPoint);

void recapPerso(Buffer *bPoint, TMP *Perso, int type);

#endif //PROJET_C_YNOV_COMBAT_H
