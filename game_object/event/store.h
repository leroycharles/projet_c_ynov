//
// Created by leroycharles on 08/03/2020.
//

#ifndef PROJET_C_YNOV_STORE_H
#define PROJET_C_YNOV_STORE_H

#include "../misc/perso.h"

int upgradeWeapon(Perso *Player, int cost);

int upgradeArmor(Perso *Player, int cost);

int upgradeShield(Perso *Player, int cost);

int upgradeXP(Perso *Player, int cost);

int upgradePotion(Perso *Player, int cost, int type);

#endif //PROJET_C_YNOV_STORE_H
