//
// Created by leroycharles on 08/03/2020.
//

#include "text.h"

/**
 * \brief Charge l'évènement texte suivant la position du joueur sur le borde et le charge dans le buffer
 */
void eventText(Board *mPoint, const Position pos, Buffer *bPoint) {
    int value = read_board(mPoint, pos);
    switch (value) {
        case 14:
            //Add Context of event 14
            for ( int i = 0, i<3, i++)
            {
                readText(14, i, bPoint);
                readAscii(14);
            }
            break;
        case 15:
            //Add Context of event 15
            for ( int i = 0, i<2, i++)
            {
                readText(15, i, bPoint);
                readAscii(15);
            }
            break;
        case 16:
            //Add Context of event 16

            for ( int i = 0, i<2, i++)
            {
                readText(16, i, bPoint);
                readAscii(16);
            }
            break;
        case 17:
            //Add Context of event 17
            for ( int i = 0, i<4, i++)
            {
                readText(17, i, bPoint);
                readAscii(17);
            }
            break;
        default:
            //Display already-came-here lines
            break;
    }
    printBuffer(bPoint);
    userIntput("", 0);
    update_board(mPoint, pos);
}