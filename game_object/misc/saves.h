//
// Created by leroycharles on 19/02/2020.
//

#ifndef PROJET_C_YNOV_SAVES_H
#define PROJET_C_YNOV_SAVES_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct stackSys stackSys;
struct stackSys {
    Board *sysBoard;
    Buffer *sysBuffer;
    Perso *sysPlayer;
    int lineNumber;
};

char *flatBoard(Board *Board);

char *flatPerso(Perso *Player);

char *flatSys(stackSys stack);

void deflatBoard(char *input, Board *mPoint);

#endif //PROJET_C_YNOV_SAVES_H
