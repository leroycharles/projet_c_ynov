//
// Created by leroycharles on 19/02/2020.
//

#include "saves.h"

char *flatBoard(Board *Board) {
    char output[256], buffer[3];

    for (int i = 0; i < 10; ++i) {
        for (int j = 0; j < 10; ++j) {
            sprintf(buffer, "%d,", Board->board[i][j]);
            strcat(output, buffer);
        }
    }

    return output;
}

char *flatPerso(Perso *Player) {
    char output[2048], buffer[8];

    sprintf(buffer, "%s,", Player->name);
    strcat(output, buffer);
    sprintf(buffer, "%d,", Player->xp);
    strcat(output, buffer);
    sprintf(buffer, "%d,", Player->money);
    strcat(output, buffer);
    sprintf(buffer, "%d,", Player->sword);
    strcat(output, buffer);
    sprintf(buffer, "%d,", Player->armor);
    strcat(output, buffer);
    sprintf(buffer, "%d,", Player->shield);
    strcat(output, buffer);
    sprintf(buffer, "%d-%d-%d,", Player->potions[0], Player->potions[1], Player->potions[2]);
    strcat(output, buffer);

    return output;
}

char *flatSys(stackSys stack) {
    char output[4096];
    strcat(output, flatPerso(stack.sysPlayer));
    strcat(output, "@");
    strcat(output, flatBoard(stack.sysBoard));
    return output;
}

void deflatBoard(char *input, Board *mPoint) {
    int k = 0;
    for (int i = 0; i < 10; ++i) {
        for (int j = 0; j < 10; ++j) {
            mPoint->board[i][j] = input[k];
            k++;
        }
    }
}

void deflatPerso(const char *input, Perso *pPoint) {
    char current, buffer[8][128], bufferL[8];
    int token = 0, i = 0, k = 0;

    while (current != EOF) {
        current = input[i];
        if (current == ',') {
            token++;
        } else {
            strcat(buffer[token], (const char *) current);
        }
        i++;
    }

    strcpy(pPoint->name, buffer[0]);
    pPoint->xp = (int) buffer[1];
    pPoint->money = (int) buffer[2];
    pPoint->sword = (int) buffer[3];
    pPoint->armor = (int) buffer[4];
    pPoint->shield = (int) buffer[5];

    i = 0;
    current = ' ';
    while (current != EOF) {
        current = buffer[token][i];
        if (current == '-') {
            pPoint->potions[k] = (int) bufferL;
            k++;
        } else {
            strcat(bufferL, (const char *) buffer[token][i]);
        }
        i++;
    }
}