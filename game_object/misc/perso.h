//
// Created by lucas on 27/02/2020.
//

#ifndef PROJET_C_YNOV_PNJ_H
#define PROJET_C_YNOV_PNJ_H

#include <stdlib.h>
#include <stdio.h>
#include "board.h"

typedef struct Perso Perso;
struct Perso {
    char name[128];
    Position position;
    int xp;
    int money;
    int sword;
    int armor;
    int shield;
    int potions[];
};

typedef struct TMP TMP;
struct TMP {
    int health;
    int attack;
    int defense;
    int level;
    int potions[];
};

TMP *initTMP(const Perso *sPerso, const int type);

int persoLevel(int XP);

void battackTMP(TMP *pTMP);

void bdefenseTMP(TMP *pTMP);

void bhealthTMP(TMP *pTMP);

int attack(const TMP *perso1, TMP *perso2);

void movePerso(Board *mPoint, Perso *Player, int movement);

#endif //PROJET_C_YNOV_PJ_H
