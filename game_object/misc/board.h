//
// Created by nicob on 26/02/2020.
//

#ifndef PROJET_C_YNOV_GAME_ENGINE_H
#define PROJET_C_YNOV_GAME_ENGINE_H

#include <stdlib.h>
#include <stdio.h>

typedef struct Board Board;
struct Board {
    char map[100][100];
    int board[10][10];
};

typedef struct Position Position;
struct Position {
    int x;
    int y;
};

void add_board(const int n, const Position pos, Board *mPoint);

void init_board(Board *mPoint, const int defaultBoard[10][10]);

int read_board(const Board *mPoint, const Position pos);

void init_map(Board *mPoint);

void update_board(Board *mPoint, const Position pos);

void caseMap(Board *mPoint, const Position pos, const int type);

void update_map(Board *mPoint, const Position posPlayer);

void initBoard(Board *iBoard, const Board *dBoard);

#endif //PROJET_C_YNOV_GAME_ENGINE_H
