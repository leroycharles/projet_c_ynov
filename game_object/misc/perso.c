//
// Created by lucas on 27/02/2020.
//

#include "perso.h"


TMP *initTMP(const Perso *sPerso, const int type) {
    TMP output;
    if (!type) //Creating a PLayer
    {
        output.level = persoLevel(sPerso->xp);
        for (int i = 0; i < 3; ++i) {
            output.potions[i] = sPerso->potions[i];
        }
    } else //Creating a Monster
    {
        int addLevel = rand() % 4;
        int singedLevel = rand() % 2;
        output.level = persoLevel(sPerso->xp) + singedLevel ? addLevel * -1 : addLevel;
    }
    output.attack = sPerso->sword * output.level;
    output.defense = (sPerso->armor + sPerso->shield) * output.level;
    output.health = output.attack + output.defense + output.level * 100;

}

int persoLevel(int XP) {
    return XP / 1000;
}

void battackTMP(TMP *pTMP) {
    pTMP->potions[1]--;
    pTMP->attack += 10 * pTMP->level;
}

void bdefenseTMP(TMP *pTMP) {
    pTMP->potions[2]--;
    pTMP->defense += 10 * pTMP->level;
}

void bhealthTMP(TMP *pTMP) {
    pTMP->potions[0]--;
    pTMP->health += 10 * pTMP->level;
}

int attack(const TMP *perso1, TMP *perso2) {
    int multAttack = rand() % 3;
    int realAttack = perso1->attack * multAttack;
    int buffer = 0;

    if (perso2->defense - realAttack > 0) {
        perso2->defense -= realAttack;
    } else {
        buffer = (perso2->defense - realAttack) * -1;
        perso2->health -= buffer;
        perso2->defense = 0;
    }

    return multAttack;
}

void movePerso(Board *mPoint, Perso *Player, int movement) {
    switch (movement) {
        case 0: //North
            Player->position.y == 0 ? (Player->position.y = 9) : (Player->position.y--);
            break;
        case 1: //South
            Player->position.y == 9 ? (Player->position.y = 0) : (Player->position.y++);
            break;
        case 3: //East
            Player->position.x == 9 ? (Player->position.x = 0) : (Player->position.x++);
            break;
        default: //West
            Player->position.x == 0 ? (Player->position.x = 9) : (Player->position.x--);
            break;
    }
}