//
// Created by nicob on 26/02/2020.
//
#include "board.h"

void add_board(const int n, const Position pos, Board *mPoint) {
    mPoint->board[pos.y][pos.x] = n;
}

void init_board(Board *mPoint, const int defaultBoard[10][10]) {
    for (int i = 0; i < 10; i++) {
        for (int j = 0; j < 10; j++) {
            mPoint->board[i][j] = defaultBoard == NULL ? 0 : defaultBoard[i][j];
        }
    }
}

int read_board(const Board *mPoint, const Position pos) {
    return mPoint->board[pos.y][pos.x];
}

void init_map(Board *mPoint) {
    for (int i = 0; i > 100; i++) {
        for (int j = 0; j > 100; j++) {
            mPoint->map[i][j] = ' ';
        }
    }
}

void update_board(Board *mPoint, const Position pos) {
    add_board(read_board(mPoint, pos) * -1, pos, mPoint);
}

void caseMap(Board *mPoint, const Position pos, const int type) {
    char caseArt[10][10];
    switch (type) {
        case 0:
            //Field
            break;

        case 1:
            //Road
            break;

        case 2:
            //Village
            break;

        case 3:
            //Perso
            break;

        case 4:
            //Store
            break;

        default:
            //Forest
            break;
    }

    for (int i = 0; i < 10; ++i) {
        for (int j = 0; j < 10; ++j) {
            mPoint->map[(pos.y * 10) + i][(pos.x * 10) + j] = caseArt[i][j];
        }
    }
}

void update_map(Board *mPoint, const Position posPlayer) {
    Position pos;
    int boardValue = 0;

    init_map(mPoint);

    for (int i = 0; i < 10; ++i) {
        for (int j = 0; j < 10; ++j) {
            pos.y = i;
            pos.x = j;
            boardValue = read_board(mPoint, pos);
            boardValue = boardValue < 0 ? -1 * boardValue : boardValue;

            switch (boardValue) {
                case 11:
                    //Forest
                    caseMap(mPoint, pos, 5);
                    break;

                case 12:
                    //Field
                    caseMap(mPoint, pos, 0);
                    break;
                case 13:
                    //Road
                    caseMap(mPoint, pos, 1);
                    break;
                default:
                    if (boardValue > 1 && boardValue < 11) //Combat elements
                    {
                        if (boardValue == 2) {
                            caseMap(mPoint, pos, 0);
                        } else if (boardValue == 9) {
                            caseMap(mPoint, pos, 1);
                        } else {
                            caseMap(mPoint, pos, 5);
                        }
                    } else if (boardValue == 1 || boardValue == 17) //Village elements
                    {
                        caseMap(mPoint, pos, 2);
                    } else //Text elements outside village
                    {
                        if (boardValue == 14) {
                            //caseMap(mPoint, pos, 2);
                        } else if (boardValue == 15) {
                            //caseMap(mPoint, pos, 2);
                        } else {
                            //caseMap(mPoint, pos, 2);
                        }
                    }
                    break;
            }
        }
    }

    caseMap(mPoint, posPlayer, 3);
}

void initBoard(Board *iBoard, const Board *dBoard) {
    if (dBoard != NULL) {
        *iBoard = *dBoard;
    } else {
        init_board(iBoard, NULL);
    }
    init_map(iBoard);
}