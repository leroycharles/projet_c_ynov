echo "Tracking Remotes"
# shellcheck disable=SC2162
git branch -r | grep -v '\->' | while read remote;
do
git branch --track "${remote#origin/}" "$remote";
done

echo "Pulling All"
# shellcheck disable=SC2162
git branch | grep -v '\->' | while read branch;
do
git checkout "${branch}" && git pull
done

echo "Pushing All"
git push -u origin --all