//
// Created by leroycharles on 07/03/2020.
//
#include "misc/ascii.c"
#include "misc/text.c"
#include "misc/buffer.c"
#include "misc/inout.c"

#include "in/console_in.c"

#include "out/console_out.c"

#include "console_interface.h"
