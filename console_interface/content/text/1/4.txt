La forêt, juste avant très calme, commence a se réveiller. Les aniamux, jusqu'ici invisible et inaudible
s'ébruite et se montre pendant leur fuite d'un enemis juque là inconnue.
Tout d'abord l'aire est devenue lourd, vous écrasant, vous coupant le souffle.
Ensuite, arriva le bruit, celui de chaine qu'on faisait trainer, mais le bruit était puissant.
Il y eu ensuite, alors que l'aura ogmentait en force et le bruit devenait de plus en plus fort,
des gémissements. Vous cherchez l'origine, votre corps crie de fuire, loin. Mais les bruits arrivent de tout côter.
Puis, plus rien. L'aura se lève, et le bruit se dissipe. Mais, quelque secondes après, vous entendez quelqu'un
appeller de l'aide. C'est alors que les arbres se sont écarté sur une main de taille humaine. Vous faites un bond
esquivant cette attaque. En face de vous, courbé sur lui même, un créature difforme, épaisse de quatres mètres
de haut, courbé, le crâne chauve, la peau verte allant vers le marron vous fait face.
Mais, quelque chose n'allait pas. La créature n'avait pas de bouche, un visage humanoïde certe
mais, si éloigné du notre, mutilé. C'est alors que vous vous êtes demandé <d'où venait la demande d'aide ?>
La réponse arriva rapidement. Au poignet de la créature, une chaine de l'épaisseur de votre bras
y était attaché, trainant au sol des corps, certans innerte, sans vie vu leur état, mais l'un gesticulait
faiblement.
