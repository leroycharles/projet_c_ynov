//
// Created by nicob on 19/02/2020.
//

#include "ascii.h"

void readAscii(int asciiCode) {
    //Declaration
    FILE *stream = NULL;

    char array[64];

    char line[128];
    sprintf(line, "console_interface/content/ascii_art/%d.txt", asciiCode);

    //Reading
    stream = fopen(line, "r");
    if (stream != NULL) {
        int i = 0;
        while (fgets(line, LINE_MAX, stream) != NULL) {
            strcpy((char *) array[i], line);
            i++;
        }
        fclose(stream);
    }
}