//
// Created by lucas on 19/02/2020.
//

#ifndef PROJET_C_YNOV_ATEXT_CONTENT_H
#define PROJET_C_YNOV_ATEXT_CONTENT_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void readText(int k, int doc, Buffer *bPoint);

#endif //PROJET_C_YNOV_ATEXT_CONTENT_H
