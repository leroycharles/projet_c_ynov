//
// Created by lucas on 19/02/2020.
//

#include "text.h"

/**
 * \brief permet de marquer un chemin et de sortir le doc correspondant
 */
void readText(int k, int doc, Buffer *bPoint) {
    //Declaration
    FILE *stream = NULL;
    char line[LINE_MAX];

    //Path
    sprintf(path, "console_interface/content/text/%d/%d.txt", k, doc);

    //Reading
    stream = fopen(path, "r");
    if (stream != NULL) {
        while (fgets(line, LINE_MAX, stream) != NULL) {
            addLineBuffer(bPoint, line);
        }
        fclose(stream);
    }
}