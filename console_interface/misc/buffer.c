//
// Created by leroycharles on 18/02/2020.
//

#include "buffer.h"

Buffer bufferStruct;
Buffer *bufferPoint = &bufferStruct;

void initBuffer(Buffer *bPoint) {
    for (int i = 0; i < 128; ++i) {
        for (int j = 0; j < 128; ++j) {
            bPoint->actual[i][j] = ' ';
            bPoint->previous[i][j] = ' ';
        }
    }
    bPoint->available = 127;
}

void printBuffer(Buffer *bPoint) {
    for (int i = 0; i < 128; ++i) {
        printf("%s", bPoint->actual[i]);
        for (int j = 0; j < 128; ++j) {
            bPoint->previous[i][j] = bPoint->actual[i][j];
            bPoint->actual[i][j] = ' ';
        }
    }
    bPoint->available = 127;
}

void addLineBuffer(Buffer *bPoint, char line[128]) {
    int bufferIndex = 0;
    while (bPoint->actual[bufferIndex][0] != ' ') {
        bufferIndex++;
    }
    for (int i = 0; i < 128; ++i) {
        bPoint->actual[bufferIndex][i] = line[i];
    }
    bPoint->available--;
}

void addArrayBuffer(Buffer *bPoint, char array[128][128]) {
    for (int i = 0; i < 128; ++i) {
        addLineBuffer(bPoint, array[i]);
    }
}

void addEmptyLine(Buffer *bPoint) {
    char line[128];
    for (int i = 0; i < 128; ++i) {
        line[i] = ' ';
    }
    addLineBuffer(bPoint, line);
}

void fillBuffer(Buffer *bPoint) {
    for (int i = bPoint->available; i != 0; --i) {
        addEmptyLine(bPoint);
        bPoint->available--;
    }
}