//
// Created by leroycharles on 18/02/2020.
//

#ifndef PROJET_C_YNOV_BUFFER_H
#define PROJET_C_YNOV_BUFFER_H

#include <stdio.h>
#include <stdlib.h>

typedef struct Buffer Buffer;

struct Buffer {
    char actual[128][128];
    char previous[128][128];
    int available;
};

void initBuffer(Buffer *bPoint);

void printBuffer(Buffer *bPoint);

void addLineBuffer(Buffer *bPoint, char line[128]);

void addArrayBuffer(Buffer *bPoint, char array[128][128]);

void fillBuffer(Buffer *bPoint);

#endif //PROJET_C_YNOV_BUFFER_H
