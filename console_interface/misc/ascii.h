//
// Created by nicob on 19/02/2020.
//

#ifndef PROJET_C_YNOV_ASCII_H
#define PROJET_C_YNOV_ASCII_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#define LINE_MAX 128

void readAscii(int asciiCode);

#endif //PROJET_C_YNOV_ASCII_H