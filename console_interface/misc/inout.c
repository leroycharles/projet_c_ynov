//
// Created by lucas on 25/02/2020.
//

#include "inout.h"

/**
 * \brief prend la saisie utilisateur
 */
int userInput(int menuNumber, Buffer *bPoint) {
    //add menu to buffer
    printBuffer(bPoint);
    int input = getchar();
    return input < 5 ? input : userInput(0, bPoint);
}
/**
 * \brief affiche le menu
 */
int menu(Perso *pPoint, Buffer *bPoint, Board *mPoint) {
    //transf osi -> id_txt
    //ASCII
    //TXT
    char line[125];
    int input = userInput(line, 0);
}