//
// Created by lucas on 25/02/2020.
//

#ifndef PROJET_C_YNOV_INPUT_OUTPUT_H
#define PROJET_C_YNOV_INPUT_OUTPUT_H

#include "../../game_object/misc/perso.h"
#include "../../game_object/misc/board.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int userInput(int menuNumber, Buffer *bPoint);

int menu(Perso *pPoint, Buffer *bPoint, Board *mPoint);

#endif //PROJET_C_YNOV_INPUT_OUTPUT_H
